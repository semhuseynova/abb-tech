package az.abbtech.exercises.homework.exam;

import java.util.Scanner;

public class MirrorNumber {
    public static void main(String[] args) {
        System.out.println("Please enter the number:");
        Scanner inputScanner = new Scanner(System.in);
        int number = inputScanner.nextInt();
        int mirrorNumber = 0;
        while (number > 0) { // 12 1
            int mod = number % 10; // qaliq 3 2 1
            mirrorNumber = mirrorNumber * 10 + mod; // 0*10 +3=3  3*10 +2=32 32*10 +1=321

            number = number / 10; // tam 12 1 0
        }
        System.out.println(mirrorNumber);
    }
}
