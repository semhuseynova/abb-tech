package az.abbtech.exercises.homework.exam;

import java.util.Scanner;

public class SumOfDigits {
    public static void main(String[] args) {
        System.out.println("Please enter the number:");
        Scanner inputScanner = new Scanner(System.in);
        int number = inputScanner.nextInt();
        int sum = 0;
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        System.out.println(sum);
    }
}
