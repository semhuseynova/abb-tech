package az.abbtech.exercises.homework.exam;

public class MinimumIndexArray {

    public static void main(String[] args) {
        int[] array = {12, 11, 99, 356, 76, 8, 87};
        int minIndex = 0;
        for (int i = 1; i < array.length; i = i + 1) {
            if (array[minIndex] > array[i]) { // min> array[2]
                minIndex = i;  // min=array[1]
            }
        }
        System.out.println(minIndex);
    }
}
