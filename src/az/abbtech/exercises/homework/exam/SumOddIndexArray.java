package az.abbtech.exercises.homework.exam;

public class SumOddIndexArray {

    public static void main(String[] args) {
        int[] array = {12, 11, 99, 356, 76, 8, 87};
        int sum = 0;
        for (int i = 1; i < array.length; i = i + 2) {
            sum = sum + array [i] ;
        }
        System.out.println(sum);
    }
}
