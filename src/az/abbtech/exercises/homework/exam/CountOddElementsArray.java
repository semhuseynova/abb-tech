package az.abbtech.exercises.homework.exam;

public class CountOddElementsArray {
    public static int CountOddElementsArray(int[] arr) {
        int count = 0;
        for (int num : arr) {
            if (num % 2 != 0) {
            count++;
            }
        }
 return count;

    }

    public static void main(String[] args) {
     int[] array = {5,3,8,9,12,15};
     int oddCount = CountOddElementsArray(array);
        System.out.println("Number of odd elements: " + oddCount);
    }
}
