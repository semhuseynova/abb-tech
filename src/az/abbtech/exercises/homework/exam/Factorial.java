package az.abbtech.exercises.homework.exam;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("Please enter the number:");
        Scanner inputScanner = new Scanner(System.in);
        int number = inputScanner.nextInt();
        int fact = 1;
        for (int i = 1; i <= number; i++) {
            fact = fact * i;
        }
        System.out.println(fact);
    }
}
