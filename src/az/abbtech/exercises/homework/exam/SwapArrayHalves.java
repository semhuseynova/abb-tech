import java.util.Arrays;

public class SwapArrayHalves {

    public static void swapHalves(int[] arr) {
        int mid = arr.length / 2;
        int[] swappedArray = new int[arr.length];

        // Copy the second half of the original array to the new array
        System.arraycopy(arr, mid, swappedArray, 0, arr.length - mid);

        // Copy the first half of the original array to the second half of the new array
        System.arraycopy(arr, 0, swappedArray, arr.length - mid, mid);

        // Update the original array with the swapped elements
        System.arraycopy(swappedArray, 0, arr, 0, arr.length);
    }

    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 4 };

        System.out.println("Original Array: " + Arrays.toString(array));

        swapHalves(array);

        System.out.println("Swapped Array: " + Arrays.toString(array));
    }
}
