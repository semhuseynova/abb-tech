package az.abbtech.exercises.homework.exam;

public class MinimumElementArray {

    public static void main(String[] args) {
        int[] array = {12, 11, 99, 356, 76, 8, 87};
        int min = array[0];
        for (int i = 1; i < array.length; i = i + 1) {
            if (min > array[i]) { // min> array[2]
                min = array[i];  // min=array[1]
            }
        }
        System.out.println(min);
    }
}
