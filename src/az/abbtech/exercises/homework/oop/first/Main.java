package az.abbtech.exercises.homework.oop.first;

public class Main{
    public static void main(String[] args) {
        Car car = new Car();
        Motorcycle motorcycle = new Motorcycle();
         Truck truck = new Truck();

         car.startEngine();
         car.drive();

         motorcycle.startEngine();
         motorcycle.drive();

         truck.startEngine();
         truck.drive();
    }
}

abstract class Vehicle {
public abstract void startEngine();
public abstract void drive();
}

class Truck {

    public void startEngine() {
        System.out.println("Truck engine started");
    }

    public void drive() {
        System.out.println("Truck is driving");
    }
}

class Car extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Car engine started");
    }

    @Override
    public void drive() {
        System.out.println("Car is driving");
    }
}
 class Motorcycle extends Vehicle {
     @Override
     public void startEngine() {
         System.out.println("Motorcycle engine started");
     }
     public void drive() {
         System.out.println("Motorcycle is driving");
     }
 }
