//Create a class to model a student.
//The class should have attributes such as name, age, and grade.
// Implement methods to set and get these attributes, and a method to display the student's information.

package az.abbtech.exercises.homework.oop.second;

public class Student {
private String name;
private int age;
private int grade;

// Constructor
    public Student (String name,int age,int grade) {
    this.name = name;
    this.age = age;
    this.grade = grade;
    }

// Getter & Setter methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
  // Method to display student's information
  public void displayInfo() {
      System.out.println("Name: "+ name);
      System.out.println("Age: " + age);
      System.out.println("Grade: " + grade);
  }
}


