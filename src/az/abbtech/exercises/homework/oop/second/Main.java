package az.abbtech.exercises.homework.oop.second;

public class Main {
public static void main (String[] args) {
    //Create a new student
    Student student1 = new Student("Semra Huseynova",31,100);
    // Display student info
    System.out.println("Student 1 info:");
    student1.displayInfo();

    //Update student attributes using setter method
    student1.setName("Sem Huseyn");
    student1.setAge(32);
    student1.setGrade(150);

    //Displaying updated student info

    System.out.println("Updated Student 1 info");
    student1.displayInfo();
   }
}
