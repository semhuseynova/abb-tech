package az.abbtech.exercises.homework.oop.oop12.task1;

abstract class Animal {
    abstract void makeSound();
}

class Dog extends Animal {
    void makeSound() {
        System.out.println("DOG: woof");
    }
}

class Cat extends Animal {

    @Override
    void makeSound() {
        System.out.println("CAT: meow");
    }
}

class Cow extends Animal {

    @Override
    void makeSound() {
        System.out.println("COW: moo");
    }
}
public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal cat = new Cat();
        Animal cow = new Cow();

        dog.makeSound();
        cat.makeSound();
        cow.makeSound();
    }
}
