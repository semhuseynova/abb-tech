package az.abbtech.exercises.homework.oop.oop12.task2;

abstract class Person {
    private String name;
    private int age;

    //constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Getter and Setter methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Abstract method
    abstract void displayInfo();
}

// Subclass Student
class Student extends Person {
    private String studentId;

    // Constructor
    public Student(String name, int age, String studentId) {
        super(name, age);
        this.studentId = studentId;
    }

    // Override displayInfo() method
    @Override
    void displayInfo() {
        System.out.println("Student Name: " + getName());
        System.out.println("Student Age: " + getAge());
        System.out.println("Student ID: " + studentId);
    }
}

// Subclass Teacher
class Teacher extends Person {
    private String subject;

    // Constructor
    public Teacher(String name, int age, String subject) {
        super(name, age);
        this.subject = subject;
    }

    // Override displayInfo() method
    @Override
    void displayInfo() {
        System.out.println("Teacher Name: " + getName());
        System.out.println("Teacher Age: " + getAge());
        System.out.println("Subject Taught: " + subject);
    }
}

    public class Main {
    public static void main(String[] args) {
        Student student = new Student("Semra", 31, "3108");
        Teacher teacher = new Teacher("Samira", 51, "English");

        System.out.println("Student Info:");
        student.displayInfo();

        System.out.println("Teacher Info:");
        teacher.displayInfo();
    }
}