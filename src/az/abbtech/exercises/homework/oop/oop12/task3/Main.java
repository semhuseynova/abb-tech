package az.abbtech.exercises.homework.oop.oop12.task3;

abstract class Media {
    private String title;
    private double duration;

    public Media(String title, double duration) {
        this.title = title;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public double getDuration() {
        return duration;
    }

    public abstract void play();
}

class Music extends Media {
    public Music(String title, double duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing music: " + getTitle());
    }
}

class Movie extends Media {
    public Movie(String title, double duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing movie: " + getTitle());
    }
}

public class Main {
    public static void main(String[] args) {
        Music music = new Music("Song Title", 3.5);
        Movie movie = new Movie("Movie Title", 120.0);

        music.play();
        movie.play();
    }
}
