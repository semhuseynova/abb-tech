package az.abbtech.exercises.homework.oop.oop12.task4;
// Logger interface
interface Logger {
    void logInfo(String message);
    void logWarning(String message);
    void logError(String message);
}

// ConsoleLogger class implementing Logger interface
class ConsoleLogger implements Logger {
    @Override
    public void logInfo(String message) {
        System.out.println("INFO: " + message);
    }

    @Override
    public void logWarning(String message) {
        System.out.println("WARNING: " + message);
    }

    @Override
    public void logError(String message) {
        System.out.println("ERROR: " + message);
    }
}

// FileLogger class implementing Logger interface
class FileLogger implements Logger {
    private String fileName;

    public FileLogger(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void logInfo(String message) {
        writeToLogFile("INFO: " + message);
    }

    @Override
    public void logWarning(String message) {
        writeToLogFile("WARNING: " + message);
    }

    @Override
    public void logError(String message) {
        writeToLogFile("ERROR: " + message);
    }

    private void writeToLogFile(String logMessage) {
        // Code to write logMessage to the file
        // Here, we'll just print it to the console for demonstration
        System.out.println("Writing to file '" + fileName + "': " + logMessage);
    }
}

public class Main {
    public static void main(String[] args) {
        // Create objects of both classes
        Logger consoleLogger = new ConsoleLogger();
        Logger fileLogger = new FileLogger("log.txt");

        // Call logging methods
        consoleLogger.logInfo("This is an informational message.");
        consoleLogger.logWarning("This is a warning message.");
        consoleLogger.logError("This is an error message.");

        fileLogger.logInfo("This is an informational message.");
        fileLogger.logWarning("This is a warning message.");
        fileLogger.logError("This is an error message.");
    }
}
